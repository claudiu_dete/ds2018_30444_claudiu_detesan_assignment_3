package consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

import java.util.ArrayList;
import java.util.List;

public class ConsumerStart {

    private static final String AMPQ_URI = "amqp://hmchouvj:5oZtYegzsLJqc6d-UgPbIEpQdI3lG96N@bee.rmq.cloudamqp.com/hmchouvj";
    private static final String EXCHANGE_NAME = "Exchange";

    public static Channel initConnection(){
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri(AMPQ_URI);
            Connection connection = factory.newConnection();
            return connection.createChannel();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            List<String> emails=new ArrayList<>();
            emails.add("claudiu.dete@gmail.com");
            Channel channel = initConnection();
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, EXCHANGE_NAME, "");
            Consumer consumer = new MessageConsumer(channel,emails);
            channel.basicConsume(queueName, true, consumer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
