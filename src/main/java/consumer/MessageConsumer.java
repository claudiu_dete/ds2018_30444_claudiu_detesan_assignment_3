package consumer;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import consumer.service.FileService;
import consumer.service.MailService;

import java.io.IOException;
import java.util.List;

public class MessageConsumer extends DefaultConsumer {

    private MailService mailService;
    private FileService fileService;
    private List<String> emails;

    public MessageConsumer(Channel channel,List<String> emails) {
        super(channel);
        this.mailService = new MailService("claudiu.dete@gmail.com","9cnctwcr$");
        this.fileService = new FileService();
        this.emails=emails;
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body)
            throws IOException {
        String message = new String(body, "UTF-8");
        fileService.writeInfoInFile(message);
        emails.stream().forEach(s->mailService.sendMail(s,"New DVD",message));

    }
}
