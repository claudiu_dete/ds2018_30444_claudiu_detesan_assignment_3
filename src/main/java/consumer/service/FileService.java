package consumer.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileService {

    private static final String START_FILE = "output";
    private static final String FILE_EXTENSION = ".txt";
    private int current;

    public FileService() {
        this.current = 0;
    }

    public void writeInfoInFile(String content) {
        String fileName = START_FILE + current + FILE_EXTENSION;
        File file = new File(fileName);
        current++;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(content);
        } catch (IOException e) {
            System.out.println("File error");
            e.printStackTrace();
        }
        System.out.println("Finished writing in file");

    }

}
