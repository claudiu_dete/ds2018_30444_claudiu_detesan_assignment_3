package producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import entity.Dvd;

public class Producer {

    private static final String AMPQ_URI="amqp://hmchouvj:5oZtYegzsLJqc6d-UgPbIEpQdI3lG96N@bee.rmq.cloudamqp.com/hmchouvj";
    //private static final String QUEUE_NAME = "Queue";
    private static final String EXCHANGE_NAME="Exchange";

    public void sendDvd(Dvd dvd){
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri(AMPQ_URI);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            String message = dvd.toString();
            channel.basicPublish(EXCHANGE_NAME,"", null, message.getBytes());
            System.out.println("Sent '" + message + "'");
            channel.close();
            connection.close();
        }
        catch(Exception e){
            System.out.println("Connection error");
            e.printStackTrace();
        }
    }
}
