package producer;

import entity.Dvd;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Dvd")
public class ProducerServlet extends HttpServlet {

    Producer producer;

    public void init(){
        producer = new Producer();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        printForm(out);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("dvdName");
        int year = Integer.parseInt(request.getParameter("dvdYear"));
        double price = Double.parseDouble(request.getParameter("dvdPrice"));
        Dvd dvd = new Dvd(title, year, price);
        producer.sendDvd(dvd);
        response.sendRedirect("/Dvd");
    }

    private void printForm(PrintWriter out) {
        out.println("<form method=\"POST\">");
        out.println("Dvd Name:<br>\n" +
                "  <input type=\"text\" name=\"dvdName\"><br>");
        out.println("Dvd production year:<br>\n" +
                "  <input type=\"text\" name=\"dvdYear\"><br>");
        out.println("Dvd price:<br>\n" +
                "  <input type=\"number\" name=\"dvdPrice\"><br>");
        out.println("<input type=\"submit\" name=\"action\" value=\"Submit\"><br><br>");
        out.println("</form>");
    }
}
